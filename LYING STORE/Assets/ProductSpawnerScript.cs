﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductSpawnerScript : MonoBehaviour
{
	public GameObject startPos;
	public GameObject ProductPrefab;

	// Update is called once per frame
	void Update()
	{
		if (!GameObject.FindGameObjectWithTag("PRODUCT"))
		{
			GameObject go = Instantiate(ProductPrefab);

			go.transform.SetParent(startPos.transform);
			go.transform.position = startPos.transform.position;
		}

	}
}
