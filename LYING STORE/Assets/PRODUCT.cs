﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Product
{
	public string Name;
	public int Code;
	public float Price;
}
