﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerScript : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "PRODUCT")
		{
			GetComponent<AudioSource>().Play();

			GameObject go = GameObject.FindGameObjectWithTag("CUSTOMER");

			go.GetComponent<CustomerScript>().WriteShopList();
			go.GetComponent<CustomerScript>().Index++;
		}
	}
}
