﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveScript : MonoBehaviour
{
	public bool MoveLeft = false;
	public bool MoveRight = true;
	public float speed = 2f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W)) transform.position = transform.position + Vector3.up * speed * Time.deltaTime;
        else if (Input.GetKey(KeyCode.A)) transform.position = transform.position + Vector3.left * speed * Time.deltaTime;
        else if (Input.GetKey(KeyCode.S)) transform.position = transform.position + Vector3.down * speed * Time.deltaTime;
        else if (Input.GetKey(KeyCode.D)) transform.position = transform.position + Vector3.right * speed * Time.deltaTime;

	}
}
