﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsPanelScript : MonoBehaviour
{
	public GameObject MONEY;
	public GameObject PROFIT;
	public GameObject CashIn;
	public GameObject CashIn2;
	public GameObject ChangeLiar;

	void Start()
	{
		MONEY.SetActive(false);
		CashIn.SetActive(false);
		CashIn2.SetActive(false);
	}

	public void OpenButton_Click()
	{
		MONEY.SetActive(true);
	}

	public void CloseButton_Click()
	{
		MONEY.SetActive(false);
	}

	public void PrintReceiptButton_Click()
	{

	}

	IEnumerator AnimateMoney()
	{
		CashIn.SetActive(true);
		CashIn2.SetActive(true);

		yield return new WaitForSeconds(1.8f);

		CashIn.SetActive(false);
		CashIn2.SetActive(false);
		MONEY.SetActive(false);

		ChangeLiar.GetComponent<Text>().text = "";
		PROFIT.GetComponent<Text>().text = "";

		StopAllCoroutines();
	}

	public void GiveChangeButton_Click()
	{
		if (MONEY.activeInHierarchy)
		{
			if (ChangeLiar.GetComponent<Text>().text.Length > 0)
			{
				double actualChange = Convert.ToDouble(ChangeLiar.GetComponent<Text>().text);
				GameObject.FindGameObjectWithTag("CUSTOMER").GetComponent<CustomerScript>().GiveChange(actualChange);
			}
			else GameObject.FindGameObjectWithTag("CUSTOMER").GetComponent<CustomerScript>().GiveChange(GameObject.FindGameObjectWithTag("CUSTOMER").GetComponent<CustomerScript>().ExpectedChange);
			StartCoroutine(AnimateMoney());
		}
	}
}
