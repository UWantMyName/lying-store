﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerScript : MonoBehaviour
{
	public GameObject ProductList;
	public GameObject TOTAL;
	public GameObject TOTALSUM;
	public GameObject CASHIN;
	public GameObject CASHINSUM;
	public GameObject CHANGE;
	public GameObject CHANGESUM;

	public GameObject DialogueBubble;
	public GameObject TextDialogue;

	private int numberOfProducts;
	private float amountMoney;
	public float ExpectedChange;
	private float GivenChange;

	Product[] ShopList = new Product[12];
	public int listLength = 0;
	bool noticed = false;

	public int Index = 1;
	public float TotalSum = 0;

	bool written = false;

	void Start()
	{
		Initialize();
	}

	private void Deactivate()
	{
		int i;

		for (i = 1; i <= 10; i++)
			GameObject.Find("MAIN CANVAS/ComputerPanel/ProductPanel/ITEM" + i.ToString()).SetActive(false);
	}

	public void Initialize()
	{
		Deactivate();

		ProductList = GameObject.Find("ProductList");
		TOTAL = GameObject.Find("TOTAL");
		TOTALSUM = GameObject.Find("TOTAL SUM");
		CASHIN = GameObject.Find("CASH IN");
		CASHINSUM = GameObject.Find("CASH IN SUM");
		CHANGE = GameObject.Find("CHANGE");
		CHANGESUM = GameObject.Find("CHANGE SUM");

		DialogueBubble = GameObject.Find("DialogueBubble");
		TextDialogue = GameObject.Find("DialogueBubble/Text");

		TextDialogue.GetComponent<Text>().text = "...";

		TOTALSUM.GetComponent<Text>().text = "0.00 $";
		CASHINSUM.GetComponent<Text>().text = "0.00 $";
		CHANGESUM.GetComponent<Text>().text = "0.00 $";

		ShopList = new Product[12];
		written = false;
		ExpectedChange = 0f;
		GivenChange = 0f;
		numberOfProducts = Random.Range(1, 10);
		listLength = 0;

		Index = 1;
		TotalSum = 0f;
		noticed = false;

		amountMoney = Random.Range(50, 100);

		GameObject.FindGameObjectWithTag("PRODUCT").GetComponent<MoveScript>().canMove = true;
	}

	void ChooseItem()
	{
		int id = Random.Range(1, 100);

		listLength++;
		ShopList[listLength] = ProductList.GetComponent<ShopProductScript>().GetItemWithId(id);
	}

	public void WriteShopList()
	{
		string path = "MAIN CANVAS/ComputerPanel/ProductPanel/ITEM";

		GameObject.Find(path + Index.ToString()).SetActive(true);

		GameObject txt = GameObject.Find(path + Index.ToString() + "/NAME");
		GameObject pr = GameObject.Find(path + Index.ToString() + "/PRICE");

		txt.GetComponent<Text>().text = ShopList[Index].Name;
		pr.GetComponent<Text>().text = ShopList[Index].Price.ToString() + " $";

		TotalSum += ShopList[Index].Price;

		TOTAL.SetActive(true);
		TOTALSUM.SetActive(true);
		

		CASHIN.SetActive(true);
		CASHINSUM.SetActive(true);
		

		CHANGE.SetActive(true);
		CHANGESUM.SetActive(true);
		
		ExpectedChange = amountMoney - TotalSum;

		if (Index == listLength)
		{
			Index++;
			TOTALSUM.GetComponent<Text>().text = TotalSum.ToString() + "$";
			CASHINSUM.GetComponent<Text>().text = amountMoney.ToString() + "$";
			CHANGESUM.GetComponent<Text>().text = (amountMoney - TotalSum).ToString() + "$";
			GameObject.Find("CHANGE LIAR").GetComponent<ChangeLiarScript>().RealChange = amountMoney - TotalSum;
		}
	}

	public void GiveChange(double change)
	{
		GivenChange = (float)change;
		CheckChange();
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(1.5f);
		Initialize();
		StopAllCoroutines();
	}

	public void CheckChange()
	{
		if (GivenChange == ExpectedChange)
		{
			DialogueBubble.SetActive(true);
			if (!noticed) TextDialogue.GetComponent<Text>().text = "Thank you!!";
			else TextDialogue.GetComponent<Text>().text = "Don't trick me next time.";
			StartCoroutine(Wait());
		}
		else if (GivenChange < ExpectedChange)
		{
			int x = Random.Range(1, 100);
			if (x % 2 == 0)
			{
				DialogueBubble.SetActive(true);
				TextDialogue.GetComponent<Text>().text = "Hey!! That is not the actual change!";
				noticed = true;
			}
			else
			{
				DialogueBubble.SetActive(true);
				TextDialogue.GetComponent<Text>().text = "Thank you!!";
				StartCoroutine(Wait());
			}
		}
	}

	void FixedUpdate()
	{
		if (numberOfProducts > 0)
		{
			ChooseItem();
			numberOfProducts--;
		}
	}
}
