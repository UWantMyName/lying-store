﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveScript : MonoBehaviour
{
	public GameObject end;
	private Vector3 currentpos;

	public bool canMove = true;
	GameObject customer;

	public GameObject Cash;
	public GameObject ButtonsPanel;

	void Start()
	{
		GetComponent<Animator>().enabled = false;
		currentpos = transform.position;
		canMove = true;
		customer = GameObject.FindGameObjectWithTag("CUSTOMER");

		Cash.SetActive(false);
		ButtonsPanel.SetActive(false);
	}

	IEnumerator Reinitialize()
	{
		GetComponent<Animator>().enabled = true;
		//GetComponent<Animator>().Play("Checked");
		yield return new WaitForSeconds(0.4f);

		GetComponent<Animator>().enabled = false;
		Color cl = new Color();
		cl.r = 255;
		cl.b = 255;
		cl.g = 255;
		cl.a = 1;

		GetComponent<Image>().color = cl;
		transform.position = currentpos;

		if (customer.GetComponent<CustomerScript>().Index > customer.GetComponent<CustomerScript>().listLength) canMove = false;

		StopAllCoroutines();
	}

    // Update is called once per frame
    void FixedUpdate()
    {
		if (transform.position.x < end.transform.position.x && canMove) transform.position = transform.position + Vector3.right * 6f * Time.deltaTime;
		else StartCoroutine(Reinitialize());

		if (!canMove)
		{
			Cash.SetActive(true);
			ButtonsPanel.SetActive(true);
		}
	}
}
