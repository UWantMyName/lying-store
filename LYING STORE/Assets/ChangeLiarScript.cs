﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLiarScript : MonoBehaviour
{
	public GameObject Profit;
	public GameObject ChangeSum;
	public GameObject Text;
	public float RealChange;

    // Update is called once per frame
    void Update()
    {
		if (Text.GetComponent<Text>().text.Length != 0)
		{
			ChangeSum.GetComponent<Text>().text = Text.GetComponent<Text>().text + " $";
			Profit.GetComponent<Text>().text = "PROFIT: " + (RealChange - (float)Convert.ToDouble(Text.GetComponent<Text>().text)).ToString() + " $";
		}
		else
		{
			ChangeSum.GetComponent<Text>().text = RealChange.ToString() + " $";
			Profit.GetComponent<Text>().text = "";
		}
	}
}
