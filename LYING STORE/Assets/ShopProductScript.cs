﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopProductScript : MonoBehaviour
{
	Product[] List = new Product[102];

	void InitializeProduct(Product product, string name, int code, float price)
	{
		product.Name = name;
		product.Code = code;
		product.Price = price;
	}

	public Product GetItemWithId(int currentCode)
	{
		int i;

		for (i = 1; i <= List.Length; i++)
			if (List[i].Code == currentCode)
			{
				Product pr = new Product();

				pr.Name = List[i].Name;
				pr.Code = List[i].Code;
				pr.Price = List[i].Price;

				return pr;
			}
		return List[1];
	}

	void InitString()
	{
		int i;

		for (i = 1; i <= 100; i++)
			List[i] = new Product();
	}

	// Start is called before the first frame update
	void Start()
	{
		InitString();

		InitializeProduct(List[1], "Apple", 1, 0.25f);
		InitializeProduct(List[2], "Orange", 2, 0.3f);
		InitializeProduct(List[3], "Banana", 3, 0.3f);
		InitializeProduct(List[4], "Pear", 4, 0.25f);
		InitializeProduct(List[5], "Pineapple", 5, 7f);
		InitializeProduct(List[6], "Melon", 6, 4f);
		InitializeProduct(List[7], "Nectarine", 7, 0.25f);
		InitializeProduct(List[8], "Peach", 8, 0.3f);
		InitializeProduct(List[9], "Strawberry", 9, 0.05f);
		InitializeProduct(List[10], "Blackberry", 10, 0.05f);
		InitializeProduct(List[11], "Blueberry", 11, 0.05f);
		InitializeProduct(List[12], "Apricot", 12, 0.15f);
		InitializeProduct(List[13], "Cherry", 13, 0.02f);
		InitializeProduct(List[14], "Coconut", 14, 2f);
		InitializeProduct(List[15], "Fig", 15, 0.1f);
		InitializeProduct(List[16], "Kiwi", 16, 0.15f);
		InitializeProduct(List[17], "Lemon", 17, 0.3f);
		InitializeProduct(List[18], "Lime", 18, 0.3f);
		InitializeProduct(List[19], "Mango", 19, 1.5f);
		InitializeProduct(List[20], "Carrot", 20, 0.1f);
		InitializeProduct(List[21], "Potato", 21, 0.1f);
		InitializeProduct(List[22], "Avocado", 22, 0.3f);
		InitializeProduct(List[23], "Onion", 23, 0.15f);
		InitializeProduct(List[24], "Eggplant", 24, 0.3f);
		InitializeProduct(List[25], "Broccoli", 25, 0.5f);
		InitializeProduct(List[26], "Cauliflower", 26, 1f);
		InitializeProduct(List[27], "Tomato", 27, 0.15f);
		InitializeProduct(List[28], "Cucumber", 28, 0.3f);
		InitializeProduct(List[29], "Cabbage", 29, 0.6f);
		InitializeProduct(List[30], "Asparagus", 30, 0.25f);
		InitializeProduct(List[31], "Pepper", 31, 0.3f);
		InitializeProduct(List[32], "Chilli", 32, 0.25f);
		InitializeProduct(List[33], "Celery", 33, 0.5f);
		InitializeProduct(List[34], "Ginger", 34, 0.8f);
		InitializeProduct(List[35], "Mushroom", 35, 0.1f);
		InitializeProduct(List[36], "Spring Onion", 36, 0.2f);
		InitializeProduct(List[37], "Corn", 37, 0.4f);
		InitializeProduct(List[38], "Lettuce", 38, 1f);
		InitializeProduct(List[39], "Apple Pie", 39, 9f);
		InitializeProduct(List[40], "Pear Tart", 40, 12f);
		InitializeProduct(List[41], "Bread", 41, 1f);
		InitializeProduct(List[42], "Cookie", 42, 0.4f);
		InitializeProduct(List[43], "Cake", 43, 15f);
		InitializeProduct(List[44], "Pound Cake", 44, 7f);
		InitializeProduct(List[45], "Croissant", 45, 1.2f);
		InitializeProduct(List[46], "Donut", 46, 1f);
		InitializeProduct(List[47], "Pizza", 47, 8.5f);
		InitializeProduct(List[48], "Muffin", 48, 0.8f);
		InitializeProduct(List[49], "Bagel", 49, 0.5f);
		InitializeProduct(List[50], "Brownie", 50, 1.45f);
		InitializeProduct(List[51], "Butter", 51, 2.3f);
		InitializeProduct(List[52], "Ice Cream", 52, 3.2f);
		InitializeProduct(List[53], "Raw Milk", 53, 2f);
		InitializeProduct(List[54], "Whole Milk", 54, 2.5f);
		InitializeProduct(List[55], "Cheese", 55, 1.8f);
		InitializeProduct(List[56], "Cream", 56, 1.5f);
		InitializeProduct(List[57], "Double Cream", 57, 2.5f);
		InitializeProduct(List[58], "Eggs", 58, 0.75f);
		InitializeProduct(List[59], "Yogurt", 59, 1.2f);
		InitializeProduct(List[60], "Ham", 60, 3f);
		InitializeProduct(List[61], "Raw Beef", 61, 8f);
		InitializeProduct(List[62], "Raw Pork", 62, 6.5f);
		InitializeProduct(List[63], "Raw Chicken", 63, 6f);
		InitializeProduct(List[64], "Burger Blend", 64, 7f);
		InitializeProduct(List[65], "Chicken Breast", 65, 2f);
		InitializeProduct(List[66], "Chicken Thigh", 66, 2.2f);
		InitializeProduct(List[67], "Chicken Wing", 67, 1f);
		InitializeProduct(List[68], "Chicken Drum", 68, 1.5f);
		InitializeProduct(List[69], "Raw Lamb", 69, 9f);
		InitializeProduct(List[70], "Raw Turkey", 70, 7f);
		InitializeProduct(List[71], "Cheese Chips", 71, 0.9f);
		InitializeProduct(List[72], "Salt Chips", 72, 0.8f);
		InitializeProduct(List[73], "Paprika Chips", 73, 0.9f);
		InitializeProduct(List[74], "Biscuits", 74, 0.7f);
		InitializeProduct(List[75], "Crackers", 75, 1f);
		InitializeProduct(List[76], "Chocolate Bar", 76, 1.4f);
		InitializeProduct(List[77], "Cereal Bar", 77, 1.3f);
		InitializeProduct(List[78], "Waffer", 78, 0.6f);
		InitializeProduct(List[79], "Waffle", 79, 0.8f);
		InitializeProduct(List[80], "Peach Jelly", 80, 1.6f);
		InitializeProduct(List[81], "Berry Jelly", 81, 1.6f);
		InitializeProduct(List[82], "Peanut Butter", 82, 3f);
		InitializeProduct(List[83], "Fruit Jam", 83, 2.5f);
		InitializeProduct(List[84], "Cereals", 84, 1.7f);
		InitializeProduct(List[85], "Honey", 85, 2.5f);
		InitializeProduct(List[86], "Water", 86, 1f);
		InitializeProduct(List[87], "Apple Juice", 87, 1.2f);
		InitializeProduct(List[88], "Orange Juice", 88, 1.4f);
		InitializeProduct(List[89], "Peach Juice", 89, 1.4f);
		InitializeProduct(List[90], "Berry Juice", 90, 1.4f);
		InitializeProduct(List[91], "Beer", 91, 2.6f);
		InitializeProduct(List[92], "Wine", 92, 10f);
		InitializeProduct(List[93], "Champagne", 93, 14f);
		InitializeProduct(List[94], "Flour", 94, 1.3f);
		InitializeProduct(List[95], "Baking Powder", 95, 0.4f);
		InitializeProduct(List[96], "Sugar", 96, 1.75f);
		InitializeProduct(List[97], "Powdered Sugar", 97, 1.3f);
		InitializeProduct(List[98], "Yeast", 98, 0.3f);
		InitializeProduct(List[99], "Food Colouring", 99, 1.5f);
		InitializeProduct(List[100], "Rice", 100, 2f);
	}
}
